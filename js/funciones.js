var map;
var doc = $(document);

map = new GMaps({
	el: '#map',
	lat: 19.35335,
	lng: -99.281972,
	zoom: 16
});


function controles () {
	map.removePolygons();
	map.removePolylines();

	// Mostrar si está checked, ocultar si no
	$( "input[type='checkbox']" ).each(function() {
		var este = $(this);
		// Ver qué tipo de control es
		var tipo_control = este.parent('div').attr("data-control");
		// Ver qué dato es
		var cambio = este.attr("data-"+tipo_control);
		// Seleccionar todos los elementos que tengan el valor del tipo de control
		var el = $('div[data-'+tipo_control+'="'+cambio+'"]');
		var ap_encontramos = 1;
		// MAPAS
		//encontramos
		if ( este.is(':checked') && cambio == "encontramos" ) {
			
			$(".js-subencontramos input[type='checkbox']").attr('checked', true);
			ap_encontramos = 1;
			el.show();
			$(".js-subencontramos").show();

		} else if ( !este.is(':checked') && cambio == "encontramos" ) {

			ap_encontramos = 0;
			el.hide();
			$(".js-subencontramos input[type='checkbox']").attr('checked', false);
			$(".js-subencontramos").hide();

		}
		// mejorariamos
		if ( este.is(':checked') && cambio == "mejorariamos" ) {
			el.show();
			mejorariamos();
			$(".js-submejorariamos").show();
		} else if ( !este.is(':checked') && cambio == "mejorariamos" ){
			el.hide();
			$(".js-submejorariamos").hide();
		}
		// vemos
		if ( este.is(':checked') && cambio == "vemos" ) {
			el.show();
			$(".js-subvemos").show();
		} else if ( !este.is(':checked') && cambio == "vemos" ) {
			el.hide();
			$(".js-subvemos").hide();
		}

		if (ap_encontramos == 1) {
			// TIPOS
			if ( este.is(':checked') && cambio == "edificios" ) {
				el.show();
				$(".js-subedificios input[type='checkbox']").attr('checked', true);
				$(".js-subedificios").show();
			} else if ( !este.is(':checked') && cambio == "edificios" ) {
				el.hide();
				$(".js-subedificios input[type='checkbox']").attr('checked', false);
				$(".js-subedificios").hide();
			}
			if ( este.is(':checked') && cambio == "servicios" ) {
				el.show();
				$(".js-subservicios input[type='checkbox']").attr('checked', true);
				$(".js-subservicios").show();
			} else if ( !este.is(':checked') && cambio == "servicios" ) {
				el.hide();
				$(".js-subservicios input[type='checkbox']").attr('checked', false);
				$(".js-subservicios").hide();
			}
			if ( este.is(':checked') && cambio == "transporte" ) {
				el.show();
				$(".js-subtransporte input[type='checkbox']").attr('checked', true);
				$(".js-subtransporte").show();
			} else if ( !este.is(':checked') && cambio == "transporte" ) {
				el.hide();
				$(".js-subtransporte input[type='checkbox']").attr('checked', false);
				$(".js-subtransporte").hide();
			}
		}

		// CATEGORÍAS
		if ( este.is(':checked') && cambio == "escuela" ) {
			el.show();
			escuela ();
		} else if ( !este.is(':checked') && cambio == "escuela" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "supermercado" ) {
			el.show();
			supermercado ();
		} else if ( !este.is(':checked') && cambio == "supermercado" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "rest>50" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "rest>50" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "rest<50" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "rest<50" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "tiendita" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "tiendita" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "material escolar" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "material escolar" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "esparcimiento" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "esparcimiento" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "salud" ) {
			el.show();
			salud();
		} else if ( !este.is(':checked') && cambio == "salud" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "cafe" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "cafe" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "deporte" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "deporte" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "otros" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "otros" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "base de taxis" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "base de taxis" ) {
			el.hide();
		}
		if ( este.is(':checked') && cambio == "base de camiones" ) {
			el.show();
		} else if ( !este.is(':checked') && cambio == "base de camiones" ) {
			el.hide();
		}
	});

	area_trabajo();
}

function fotos () {
	/*
	map.drawOverlay({
		lat: 19.35683,
		lng: -99.27742,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/0.jpg" data-display="foto" class=".js-foto">Huellas en el asfalto</a></div>'
	});
	*/
	map.drawOverlay({
		lat: 19.35685,
		lng: -99.27744,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/11.jpg" data-display="foto" class=".js-foto">Vista del Office Depot</a></div>'
	});
	map.drawOverlay({
		lat: 19.35514,
		lng: -99.27890,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/20.jpg" data-display="foto" class=".js-foto">Vista del Sam\'s</a></div>'
	});
	map.drawOverlay({
		lat: 19.35333,
		lng: -99.28093,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/08.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.36094,
		lng: -99.28055,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/16.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.35293,
		lng: -99.28406,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/06.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.35108,
		lng: -99.28641,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/04.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.34794,
		lng: -99.28990,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/01.jpg" data-display="foto" class=".js-foto">Arteaga y Salazar</a></div>'
	});
	map.drawOverlay({
		lat: 19.35046,
		lng: -99.28908,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/02.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.35043,
		lng: -99.28905,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/03.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	
	map.drawOverlay({
		lat: 19.35061,
		lng: -99.28431,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/05.jpg" data-display="foto" class=".js-foto">Conalep</a></div>'
	});
	map.drawOverlay({
		lat: 19.35434,
		lng: -99.28279,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/07.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.35692,
		lng: -99.27788,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/10.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.35727,
		lng: -99.27758,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/12.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.35770,
		lng: -99.27683,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/13.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.35954,
		lng: -99.27862,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/14.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
	map.drawOverlay({
		lat: 19.36057,
		lng: -99.28031,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="Panorama urbano" data-categoria="foto" data-mapa="vemos"><a href="#" data-url="fotos/15.jpg" data-display="foto" class=".js-foto">foto</a></div>'
	});
}

function mapa(){
	// Comienza el mapeo de puntos
	/*
	map.drawOverlay({
		lat: 19.3533,
		lng: -99.28197,
		layer: 'floatPane',
		content: '<div class="overlay">UAM Cuajimalpa</div>'
	});
	*/
	map.drawOverlay({
		lat: 19.35630,
		lng: -99.27613,
		layer: 'floatPane',
		content: '<div class="overlay" data-categoria="rest<50" data-tipo="servicios" data-mapa="encontramos">Tacos de canasta</div>'
	});
	/*
	map.drawOverlay({
		lat: 19.35621,
		lng: -99.27635,
		layer: 'floatPane',
		content: '<div class="overlay" data-categoria="supermercado" data-tipo="servicios" data-mapa="encontramos">Superama</div>'
	});
	*/
	map.drawOverlay({
		lat: 19.35627,
		lng: -99.27622,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="cafe" data-mapa="encontramos">Starbucks</div>'
	});
	map.drawOverlay({
		lat: 19.35628,
		lng: -99.27585,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="tiendita" data-mapa="encontramos">K-mart</div>'
	});
		map.drawOverlay({
		lat: 19.35679,
		lng: -99.27739,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="peligro" data-categoria="coladeras" data-mapa="mejorariamos">Coladera abierta </div>'
	});
	map.drawOverlay({
		lat: 19.35639,
		lng: -99.27588,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="transporte" data-categoria="base de camiones" data-mapa="encontramos">Ruta WTC- club de sams</div>'
	});
	map.drawOverlay({
		lat: 19.35646,
		lng: -99.27589,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Puesto de periódicos</div>'
	});
	map.drawOverlay({
		lat: 19.35620,
		lng: -99.27588,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Banorte</div>'
	});
	map.drawOverlay({
		lat: 19.35623,
		lng: -99.27604,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="transporte" data-categoria="base de taxis" data-mapa="encontramos">Base de taxis</div>'
	});
	map.drawOverlay({
		lat: 19.35628,
		lng: -99.27586,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">DHL</div>'
	});
	map.drawOverlay({
		lat: 19.35629,
		lng: -99.27584,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Estética canina</div>'
	});
	//nueva categoría "salud"
	map.drawOverlay({
		lat: 19.35620,
		lng: -99.27587,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="salud" data-mapa="encontramos">City Farma</div>'
	});
	/*
	map.drawOverlay({
	lat: 19.35452,
	lng: -99.27734,
	layer: 'floatPane',
	content: '<div class="overlay" data-tipo="servicios" data-categoria="supermercado" data-mapa="encontramos">Sam\'s Club</div>'
	});
	*/
	map.drawOverlay({
		lat: 19.35289,
		lng: -99.27548,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest<50" data-mapa="encontramos">Puestos semifijos de tacos</div>'
	});
	map.drawOverlay({
		lat: 19.35396,
		lng: -99.27634,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Gasolinera</div>'
	});
	map.drawOverlay({
		lat: 19.35403,
		lng: -99.27636,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Banorte (cajero automtico) </div>'
	});
	map.drawOverlay({
		lat: 19.35410,
		lng: -99.27639,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Gasolinera </div>'
	});
	//
	map.drawOverlay({
		lat: 19.35363,
		lng: -99.28037,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="peligro" data-categoria="coladeras" data-mapa="mejorariamos">Coladera abierta </div>'
	});
	map.drawOverlay({
		lat: 19.35337,
		lng: -99.28080,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="peligro" data-categoria="coladeras" data-mapa="mejorariamos">Coladera abierta </div>'
	});
	map.drawOverlay({
		lat: 19.35567,
		lng: -99.27845,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="peligro" data-categoria="coladeras" data-mapa="mejorariamos">Coladera abierta </div>'
	});
	map.drawOverlay({
		lat: 19.35522,
		lng: -99.27861,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="peligro" data-categoria="coladeras" data-mapa="mejorariamos">Coladera abierta </div>'
	});
	map.drawOverlay({
		lat: 19.35452,
		lng: -99.27946,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="peligro" data-categoria="coladeras" data-mapa="mejorariamos">Coladera abierta </div>'
	});
	map.drawOverlay({
		lat: 19.35443,
		lng: -99.27955,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="peligro" data-categoria="coladeras" data-mapa="mejorariamos">Coladera abierta </div>'
	});
	map.drawOverlay({
		lat: 19.35390,
		lng: -99.27630,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Estacionamiento </div>'
	});
	map.drawOverlay({
		lat: 19.35021,
		lng: -99.28244,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="salud" data-mapa="encontramos">Dentista </div>'
	});
	map.drawOverlay({
		lat: 19.35025,
		lng: -99.28234,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Estética</div>'
	});
	map.drawOverlay({
		lat: 19.35026,
		lng: -99.28332,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Productos cárnicos</div>'
	});
	map.drawOverlay({
		lat: 19.35052,
		lng: -99.28637,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="tiendita" data-mapa="encontramos">Tiendita</div>'
	});
	map.drawOverlay({
		lat: 19.35049,
		lng: -99.28593,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="tiendita" data-mapa="encontramos">Tiendita</div>'
	});
	map.drawOverlay({
		lat: 19.35047,
		lng: -99.28575,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="salud" data-mapa="encontramos">Dentista</div>'
	});
	map.drawOverlay({
		lat: 19.35238,
		lng: -99.28464,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">Restaurantes >50</div>'
	});
	map.drawOverlay({
		lat: 19.35120,
		lng: -99.28467,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Comex</div>'
	});
	map.drawOverlay({
		lat: 19.35193,
		lng: -99.28520,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="material escolar" data-mapa="encontramos">Material escolar</div>'
	});
	map.drawOverlay({
		lat: 19.35176,
		lng: -99.28498,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Lavandería</div>'
	});
	map.drawOverlay({
		lat: 19.35138,
		lng: -99.28487,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="tiendita" data-mapa="encontramos">Tiendita</div>'
	});
	map.drawOverlay({
		lat: 19.35099,
		lng: -99.28453,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="tiendita" data-mapa="encontramos">Tiendita</div>'
	});
	map.drawOverlay({
		lat: 19.34991,
		lng: -99.28540,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Iglesia</div>'
	});
	map.drawOverlay({
		lat: 19.34993,
		lng: -99.28452,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="tiendita" data-mapa="encontramos">Tiendita</div>'
	});
	map.drawOverlay({
		lat: 19.35004,
		lng: -99.28449,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Puesto de periódicos</div>'
	});
	map.drawOverlay({
		lat: 19.35040,
		lng: -99.28407,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="tiendita" data-mapa="encontramos">Tiendita</div>'
	});
	map.drawOverlay({
		lat: 19.35021,
		lng: -99.28373,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">Restaurantes >50</div>'
	});
	map.drawOverlay({
		lat: 19.35018,
		lng: -99.28390,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="tiendita" data-mapa="encontramos">Tiendita</div>'
	});
	map.drawOverlay({
		lat: 19.34787,
		lng: -99.28991,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="deporte" data-mapa="encontramos">Yoga</div>'
	});
	map.drawOverlay({
		lat: 19.36045,
		lng: -99.28001,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Fantasías Miguel</div>'
	});
	map.drawOverlay({
		lat: 19.36104,
		lng: -99.27988,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">otros</div>'
	});
	map.drawOverlay({
		lat: 19.35647,
		lng: -99.28226,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="cafe" data-mapa="encontramos">Starbucks</div>'
	});
	map.drawOverlay({
		lat: 19.35664,
		lng: -99.28389,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="cafe" data-mapa="encontramos">Gloria Jeans</div>'
	});
	map.drawOverlay({
		lat: 19.35671,
		lng: -99.28388,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="salud" data-mapa="encontramos">Farma pronto</div>'
	});
	map.drawOverlay({
		lat: 19.35675,
		lng: -99.28386,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="tiendita" data-mapa="encontramos">Oxxo</div>'
	});
	map.drawOverlay({
		lat: 19.35137,
		lng: -99.28444,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">otros</div>'
	});
	map.drawOverlay({
		lat: 19.35341,
		lng: -99.28378,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">restaurantes >50</div>'
	});
	map.drawOverlay({
		lat: 19.35323,
		lng: -99.28398,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">restaurantes >50</div>'
	});
	map.drawOverlay({
		lat: 19.35353,
		lng: -99.28369,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">otros</div>'
	});
	map.drawOverlay({
		lat: 19.35339,
		lng: -99.28370,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest<50" data-mapa="encontramos">restaurantes <50</div>'
	});
	map.drawOverlay({
		lat: 19.35027,
		lng: -99.28309,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Comex</div>'
	});
	map.drawOverlay({
		lat: 19.35027,
		lng: -99.28321,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">otros</div>'
	});
	map.drawOverlay({
		lat: 19.35090,
		lng: -99.28172,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="deporte" data-mapa="encontramos">Yoga</div>'
	});
	/*
	map.drawOverlay({
		lat: 19.35691,
		lng: -99.27790,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="infraestructura" data-categoria="zona de descanso" data-mapa="encontramos">zona de descanso</div>'
	});
	*/
	/*
	map.drawOverlay({
		lat: 19.35609,
		lng: -99.28236,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="hospital" data-mapa="encontramos">Centro médico ABC</div>'
	});
	*/
	map.drawOverlay({
		lat: 19.35786,
		lng: -99.27765,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">Toks</div>'
	});
	map.drawOverlay({
		lat: 19.358282,
		lng: -99.277402,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">Gino s</div>'
	});
	map.drawOverlay({
		lat: 19.357779,
		lng: -99.277519,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="edificios" data-categoria="escuela" data-mapa="encontramos">Universidad del Valle de México</div>'
	});
	map.drawOverlay({
		lat: 19.35784,
		lng: -99.27761,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="material escolar" data-mapa="encontramos">Office Depot</div>'
	});
	map.drawOverlay({
		lat: 19.358298,
		lng: -99.277619,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="cafe" data-mapa="encontramos">Cielito querido</div>'
	});
	map.drawOverlay({
		lat: 19.358449,
		lng: -99.277373,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">Wa Teppan Sushi Bar</div>'
	});
	map.drawOverlay({
		lat: 19.359187,
		lng: -99.278489,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">Which which?</div>'
	});
	map.drawOverlay({
		lat: 19.359478,
		lng: -99.278462,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">La Mansión</div>'
	});
	map.drawOverlay({
		lat: 19.359978,
		lng: -99.278894,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Scotiabank</div>'
	});
	map.drawOverlay({
		lat: 19.359988,
		lng: -99.278736 ,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="esparcimiento" data-mapa="encontramos">La cervecería</div>'
	});
	map.drawOverlay({
		lat: 19.359717,
		lng: -99.278910,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="salud" data-mapa="encontramos">Farmacia Plenia</div>'
	});
	map.drawOverlay({
		lat: 19.360431,
		lng: -99.279416,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="rest>50" data-mapa="encontramos">Las alitas</div>'
	});
	map.drawOverlay({
		lat: 19.360846,
		lng: -99.279518,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="transporte" data-categoria="base de camiones" data-mapa="encontramos">RTP y Ecobus</div>'
	});
	map.drawOverlay({
		lat: 19.360689,
		lng: -99.279920,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="otros" data-mapa="encontramos">Puesto de periódicos</div>'
	});
	map.drawOverlay({
		lat: 19.360700,
		lng: -99.280258,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="salud" data-mapa="encontramos">Laboratorio Chopo</div>'
	});
	map.drawOverlay({
		lat: 19.360798,
		lng: -99.280380,
		layer: 'floatPane',
		content: '<div class="overlay" data-tipo="servicios" data-categoria="deporte" data-mapa="encontramos">Party Gym</div>'
	});

	area_trabajo();
	escuela();
	supermercado();
	otros();
	salud();
}
function area_trabajo () {
	var area = [
		[19.3490761130718063,	-99.29138389257451536],
		[19.34945808549410984,	-99.29139667432725957],
		[19.34964553243798235,	-99.29092193778875242],
		[19.34973189889622347,	-99.29079500219221188],
		[19.34992173877687449,	-99.29061983491475019],
		[19.34995538368517032,	-99.29062053192747328],
		[19.35027843494964372,	-99.29018197277629554],
		[19.35047002517751125,	-99.29002789195351397],
		[19.3505472573982189,	-99.28983142340996437],
		[19.35056655395095149,	-99.28973285253030667],
		[19.35059291685730898,	-99.28960731024575637],
		[19.35091102911933092,	-99.289282368978661],
		[19.35101069317441613,	-99.2891438272240805],
		[19.35106375862021721,	-99.28906431804479382],
		[19.35134691000795826,	-99.28867436413540304],
		[19.35160637384687021,	-99.28836500303880541],
		[19.35172626495413084,	-99.28835715270329842],
		[19.35219586136751246,	-99.28783909293564136],
		[19.35237211890992626,	-99.28812808461255202],
		[19.35239828191832956,	-99.28829068904396138],
		[19.35239702732951983,	-99.28851394462532198],
		[19.35244968340487048,	-99.28851520936838426],
		[19.35251590984019643,	-99.2884094167536233],
		[19.35257671273947366,	-99.28810087427369524],
		[19.35276987154257,	-99.28764126479137531],
		[19.35315309919321791,	-99.28685820646518323],
		[19.35340669306316741,	-99.28632002362169828],
		[19.35364902945118359,	-99.28563354577983091],
		[19.35384218208176321,	-99.2851739303192744],
		[19.35402623923088683,	-99.28501992411797517],
		[19.35424763852658714,	-99.28486295639309844],
		[19.35376721403341804,	-99.28352160586415209],
		[19.35415849997497162,	-99.2830865373339293],
		[19.35427922302911341,	-99.28302622547289502],
		[19.35438545852369785,	-99.28304759076614516],
		[19.35451170008085953,	-99.28313253682340189],
		[19.35514857607765293,	-99.28360569523547952],
		[19.35551874000939776,	-99.28382420913189321],
		[19.35584888743646204,	-99.28391858730861941],
		[19.35619350390733828,	-99.28394339196582052],
		[19.35648647884517715,	-99.28393179479616037],
		[19.35704684371393114,	-99.28373909687560683],
		[19.35800120176640959,	-99.28321723643574614],
		[19.35875428451750224,	-99.28284027541283763],
		[19.35889805957112486,	-99.28273158399753129],
		[19.35896717358588859,	-99.28261368548858457],
		[19.3590562561967765,	-99.28258357800879708],
		[19.35917697435233364,	-99.2825262901142338],
		[19.35987895258397984,	-99.2817649194228693],
		[19.36064126081070569,	-99.28099154107130175],
		[19.36094927261807541,	-99.28054117535252487],
		[19.36103282503432865,	-99.28037185488953753],
		[19.36125204150606649,	-99.27976397633761962],
		[19.36104308301081645,	-99.2793036479051807],
		[19.36087920979878874,	-99.27940928338108506],
		[19.36074988554437581,	-99.27946050724328586],
		[19.36054880941445688,	-99.27947832141244078],
		[19.36029039308564492,	-99.27942946324661477],
		[19.36008087702140301,	-99.27933227069756583],
		[19.35983424603007563,	-99.27908976196064827],
		[19.35951325459126338,	-99.27864135249741651],
		[19.35930994491585011,	-99.27824156195254091],
		[19.35913227098646416,	-99.27798101626784444],
		[19.35891144566046762,	-99.27776578948787289],
		[19.35863316384840971,	-99.27755954434459795],
		[19.35823709724120434,	-99.27737125726076783],
		[19.3580419037213467,	-99.27729830140189904],
		[19.35795875506887143,	-99.27720435252338405],
		[19.3579129531283769,	-99.27710441467266378],
		[19.35789595976950039,	-99.27694703010699584],
		[19.35795110027090971,	-99.27657188984450443],
		[19.35800302354435587,	-99.27642369963507463],
		[19.35747266313075698,	-99.27577522777458796],
		[19.35718508898753143,	-99.2760138032449504],
		[19.35680872659528262,	-99.27608882144650693],
		[19.35667949036430358,	-99.2760825518705019],
		[19.35654183496331626,	-99.27594614801002137],
		[19.35657096859409876,	-99.27567385250030441],
		[19.35696794364968198,	-99.2752629756892162],
		[19.35677574934475942,	-99.27510529843215181],
		[19.35639322220529124,	-99.27545870452465238],
		[19.35624619909023991,	-99.27581855694482726],
		[19.35527520693724313,	-99.27595914793414522],
		[19.35454266483483821,	-99.27605777437574375],
		[19.35415202429130588,	-99.27607829922688154],
		[19.35387356154003413,	-99.27599007672525033],
		[19.35368412303035157,	-99.2759095691989728],
		[19.35351199967714564,	-99.27577462307949929],
		[19.35339155379433507,	-99.27565338100831127],
		[19.35324551752123767,	-99.27547665012900779],
		[19.35310607765573465,	-99.27516098056229055],
		[19.35274436046539748,	-99.27529517590093633],
		[19.35324901753067905,	-99.2768167267883257],
		[19.35345859835065951,	-99.27689717658834923],
		[19.35363946383132117,	-99.27694892375879476],
		[19.35377999985007946,	-99.27707927877092686],
		[19.35383435374470196,	-99.27722159256468615],
		[19.3537938722503533,	-99.27740005809226886],
		[19.3530711087573124,	-99.27861830831443513],
		[19.35290449335797547,	-99.27863920730683844],
		[19.35265975167610719,	-99.27903822076704898],
		[19.35252126366655645,	-99.27944649187905668],
		[19.35248614319467819,	-99.27987309440790398],
		[19.35248526827448501,	-99.28044197600357279],
		[19.35245908364510115,	-99.28065980133555968],
		[19.35239279637113441,	-99.28080796060177704],
		[19.35214833016173586,	-99.28102541196025754],
		[19.35169416620140836,	-99.28126368502981336],
		[19.35101585025449467,	-99.2815832728838501],
		[19.35067957354441504,	-99.28173701889791403],
		[19.35048400251994138,	-99.28190916157228685],
		[19.35038616307925352,	-99.28203003091353196],
		[19.35028534363724972,	-99.28222049144424943],
		[19.35019584358161637,	-99.28251990473889066],
		[19.35019287760016837,	-99.28258041825580449],
		[19.35019834013811746,	-99.28276198353211157],
		[19.3501601987258951,	-99.28327935192385212],
		[19.35008144059719015,	-99.28405687970233373],
		[19.35003483510638844,	-99.28447437708707923],
		[19.35003186847688639,	-99.2845348905002254],
		[19.34995912401359419,	-99.28513692373341826],
		[19.34983754678277634,	-99.28574189731979516],
		[19.34969317603965777,	-99.28622881929673838],
		[19.3494018637410079,	-99.28700899754122133],
		[19.34915392617372376,	-99.28760164255572818],
		[19.34862690261616791,	-99.28850243766338224],
		[19.34821507654249828,	-99.28920372109925552],
		[19.34795576372991022,	-99.28972069115778254],
		[19.34787504115416823,	-99.28991117955879986],
		[19.34811889196195978,	-99.29008711192942371],
		[19.34842572187156051,	-99.29039326976051427],
		[19.34867191798282704,	-99.29089903084523883],
		[19.34885820453461847,	-99.29115051108161083],
		[19.34904454396269458,	-99.29136870699215933],
		[19.3490761130718063,	-99.29138389257451536]
	];

	map.drawPolygon({
		paths: area,
		strokeColor: '#131540',
		strokeOpacity: 0.6,
		strokeWeight: 2,
		fillColor: '#3498db',
		fillOpacity: 0,
		clickable: false
	});
}
function mejorariamos () {
	linea1 = [
		[19.35015316464432189,	-99.28396057198008862],
		[19.35036567624042547,	-99.28411471357769358],
		[19.35054850553017403,	-99.2842661978261134],
		[19.350506283259854,	-99.28437298463762772]
	];
	linea2 = [
		[19.3504621361759277,	-99.28412791225174772],
		[19.35040038333840684,	-99.28406785956701697],
		[19.35045718847034024,	-99.28412790368658136],
		[19.35063512697636767,	-99.28424289077547371]
	];
	linea3 = [
		[19.35142766720912633,	-99.28445537809955113],
		[19.35158590829086123,	-99.28451038586811705],
		[19.35169233685861556,	-99.28447668770651546],
		[19.35169488396744697,	-99.2844297776622966],
		[19.35162067246477235,	-99.28442704268915975],
		[19.35149703274208832,	-99.28439294578890895]
	];
	linea4 = [
		[19.34911476606491121,	-99.2913998114097609],
		[19.34917924458815719,	-99.29130088580828328],
		[19.34930344945993852,	-99.29098052891764326],
		[19.34939062499594797,	-99.29061058586168542],
		[19.34947001685197776,	-99.29046737869425954],
		[19.34981690842867152,	-99.29012135096185432],
		[19.35000795499005122,	-99.2897698339608894],
		[19.35007750050445452,	-99.28959533248227842],
		[19.35020156167447425,	-99.28936358763128567],
		[19.35072939368281908,	-99.28879633551501627],
		[19.35092770242528104,	-99.28854386890944284]
	];
	linea5 = [
		[19.3490752386289202,	-99.29136585912822],
		[19.34911490777767185,	-99.29131119689556328],
		[19.34923416912627303,	-99.29098822505517319],
		[19.34930138337354322,	-99.29072510592501999],
		[19.34932132804610561,	-99.29062870740990832],
		[19.34941312241846845,	-99.29046467168873846],
		[19.34976743969619051,	-99.29011605086697045],
		[19.34994856596698654,	-99.28978015443222205],
		[19.35000822851330327,	-99.2895978165892501],
		[19.35014467964325036,	-99.28935306201424282],
		[19.35064028543758141,	-99.28882745481563177],
		[19.35087573502632452,	-99.28855420292664746],
		[19.35099485955723608,	-99.28831462843993449]
	];
	linea6 = [
		[19.35194199785349767,	-99.28539456013504605],
		[19.35191700200945064,	-99.28555871748571349],
		[19.35186725114510153,	-99.28573325713171016],
		[19.35186472823254533,	-99.28576452907499572],
		[19.3518621644223181,	-99.28582186455871295],
		[19.35183208953537459,	-99.28606941645314521],
		[19.35181178271175284,	-99.28639778246559899],
		[19.35177914705938207,	-99.28670006325187103],
		[19.35176419728446717,	-99.28676780248373746]
	];
	linea7 = [
		[19.35293115068208536,	-99.28406181456486479],
		[19.35263157470503259,	-99.28421507206441277],
		[19.35233944049615573,	-99.284355310060036],
		[19.35222307585491919,	-99.2844150548818476],
		[19.35207704531683959,	-99.2844617163720784],
		[19.35189642560778722,	-99.28447964783477175],
		[19.35178509411188585,	-99.28448466757166102],
		[19.35169109586226099,	-99.28447929191418098],
		[19.35169601100442804,	-99.2845001513049823],
		[19.3518987651224954,	-99.28456566183868404]
	];
	linea8 = [
		[19.35416240759439788,	-99.2829379818935962],
		[19.35413260394439305,	-99.28301351604194736],
		[19.35375094175067545,	-99.28345594438886224],
		[19.35305259698833424,	-99.28391606747830167],
		[19.35251777343722779,	-99.2842174813469569],
		[19.35221820953019289,	-99.28436291910472278]
	];
	linea9 = [
		[19.35313256208052124,	-99.28115117181829419],
		[19.35323597915707694,	-99.28088986686192641],
		[19.3533976843694937,	-99.28063373129634783],
		[19.35361257939651836,	-99.28029958007840605],
		[19.35387925302720902,	-99.27998856435652897],
		[19.35413264404399669,	-99.27971805201447353],
		[19.35446790518752636,	-99.2794026146786166],
		[19.35522785366291387,	-99.27875054873059923],
		[19.35562414224171235,	-99.27844357100353534],
		[19.35683922244434285,	-99.27746416833741705]
	];
	linea10 = [
		[19.35509213265582673,	-99.27866521947125023],
		[19.35490335953761942,	-99.27845027789732057]
	];
	linea11 = [
		[19.35797109577843855,	-99.27745239467607519],
		[19.35778232266023124,	-99.27723745310214554]
	];

	
	map.drawPolyline({
		path: linea1,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea2,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea3,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea4,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea5,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea6,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea7,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea8,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea9,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea10,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});
	map.drawPolyline({
		path: linea11,
		strokeColor: '#ff0000',
		strokeOpacity: 1,
		strokeWeight: 2
	});

	entrada_gas = [
		[19.35421302863852588,	-99.27637960590196542],
		[19.35420389309263811,	-99.27637476453270438],
		[19.35380192856665005,	-99.27627793714751192],
		[19.35376081850247942,	-99.27631666810160027],
		[19.35417191867796305,	-99.27646675054863579],
		[19.35421302863852588,	-99.27637960590196542]
	];
	peligroso_peaton = [
		[19.35571125124280556,	-99.27616658565456476],
		[19.35538237428905006,	-99.27621499934716098],
		[19.35539150976893552,	-99.27630698536307818],
		[19.35571581897361426,	-99.27627793714751192],
		[19.35571125124280556,	-99.27616658565456476]
	];
	baldio_1 = [
		[19.35353289382300446,	-99.28367971712893336],
		[19.35424361425481266,	-99.28480169751109941],
		[19.35529686323367926,	-99.28519448799623603],
		[19.35603457667157912,	-99.28487257321818049],
		[19.35612510863277791,	-99.28392920302040636],
		[19.35585053551259449,	-99.28391308933261428],
		[19.35552659909138384,	-99.28382391054077516],
		[19.355158324070878,	-99.28361215398123818],
		[19.35475556082091231,	-99.28330650813978764],
		[19.35451091291731984,	-99.28313666949975413],
		[19.35444419183684062,	-99.28308963912382978],
		[19.35432058429816848,	-99.28303469146051441],
		[19.35426616360443575,	-99.283031991193198],
		[19.35418944179130563,	-99.28305271005133648],
		[19.35378299635982202,	-99.28352376595567819],
		[19.35353289382300446,	-99.28367971712893336]
	];
	baldio_2 = [
		[19.35416242378860829,	-99.28292755632824651],
		[19.35338641079797029,	-99.28242579225491227],
		[19.35239335214961187,	-99.28309652672770369],
		[19.35146960761602131,	-99.28373609671882605],
		[19.35165204303049435,	-99.28414039765455357],
		[19.35191173248504626,	-99.2841825490638854],
		[19.35178763308382699,	-99.28444297020764964],
		[19.35190390821393791,	-99.28444056537271933],
		[19.3520548498308429,	-99.2844173697022967],
		[19.35222809680195866,	-99.28436814897145268],
		[19.35252519906047297,	-99.28421488783517646],
		[19.35306744010116731,	-99.28391609315833932],
		[19.35375340749078532,	-99.28346116143151789],
		[19.35414002550053425,	-99.28301352884190578],
		[19.35416242378860829,	-99.28292755632824651]
	];
	barranca = [
		[19.34911723991649168,	-99.29139981580162555],
		[19.34933473871133813,	-99.29152530529792386],
		[19.34940665553281747,	-99.29141596753696319],
		[19.34951590088190443,	-99.29116856080928244],
		[19.34962023565061884,	-99.29089768812256978],
		[19.3497045504762859,	-99.29077012757555565],
		[19.34993742518576099,	-99.29056203373789913],
		[19.35031423975449627,	-99.29006749700039336],
		[19.350452957998165,	-99.28995306328225467],
		[19.35052492364341958,	-99.28981244809708073],
		[19.3505399698737115,	-99.28968476402690158],
		[19.35059460178269575,	-99.28955454342998621],
		[19.35089444294222005,	-99.28923709829835786],
		[19.35113491938335528,	-99.28891433480353612],
		[19.35135804526475312,	-99.28861239067313704],
		[19.35143241769659639,	-99.28851348001106203],
		[19.35106664662932374,	-99.28828608471853556],
		[19.35103186832460409,	-99.28837724579635449],
		[19.35083350612555009,	-99.28866359514807982],
		[19.35073434138624648,	-99.28879634422104061],
		[19.35019910024184497,	-99.28935576428909826],
		[19.35007749221831119,	-99.28960054513214573],
		[19.35001539312692387,	-99.28975942176779768],
		[19.34982431753196863,	-99.29012918304255209],
		[19.34946754300030136,	-99.29046737431637837],
		[19.34939557269930077,	-99.29061059462176786],
		[19.34931091263990766,	-99.29095447894998472],
		[19.34918420896003965,	-99.29129046934764347],
		[19.34911723991649168,	-99.29139981580162555]
	];
	zona_deshabitada = [
		[19.35459316374847916,	-99.2821692549723025],
		[19.35484234868495079,	-99.28170015702556839],
		[19.35506794672125253,	-99.28118115593134974],
		[19.35541608212052012,	-99.28047953788110647],
		[19.35536173851654596,	-99.27991434935310622],
		[19.35518437053088192,	-99.27984756500801211],
		[19.35481710065261396,	-99.28017103652793196],
		[19.35440636542124082,	-99.28054844848419691],
		[19.35400349134144804,	-99.2809424923352708],
		[19.35370698939494716,	-99.28140735285954577],
		[19.35399816750360102,	-99.28183582544072294],
		[19.35441585451502533,	-99.28206507281102233],
		[19.3545931637484791,	-99.28216925497230256]
	];
	zona_peligro_vasco = [
		[19.35336130408656174,	-99.28238431716417267],
		[19.35343781309490652,	-99.28226347029311682],
		[19.35313391183172271,	-99.28175822826413821],
		[19.35315475228015814,	-99.28132269183400638],
		[19.35348570249923483,	-99.28060153796999998],
		[19.35370073852238448,	-99.28033784012777119],
		[19.35401092711242299,	-99.27994064825541898],
		[19.35433163648089305,	-99.27966227452593273],
		[19.35472708059211655,	-99.27929105970163448],
		[19.3557120181057094,	-99.27845854600147391],
		[19.35690414115052604,	-99.27747128194796744],
		[19.35683194654192008,	-99.27744874467337866],
		[19.35562414224171235,	-99.27842814733952537],
		[19.35526465682971065,	-99.27868024491023391],
		[19.354527383188465,	-99.27930056619800325],
		[19.35417728792147685,	-99.27963913902993909],
		[19.35392389695178039,	-99.27989407815260847],
		[19.35366478164888449,	-99.28019723248708317],
		[19.35339040831356527,	-99.28065686679238411],
		[19.35324367853213445,	-99.28081896482011359],
		[19.35305839039574138,	-99.28113559859889392],
		[19.35302158670514672,	-99.28176279943346572],
		[19.3533613040865617,	-99.282384317164172674]
	];
	cruce_peligroso = [
		[19.35086729484126522,	-99.28448307666633355],
		[19.35091062796062644,	-99.2844570883136015],
		[19.35090703925590461,	-99.28437889170332653],
		[19.35089226530059747,	-99.28433455822026588],
		[19.3508328765652351,	-99.28434488071378894],
		[19.35081060985735846,	-99.28434614530924307],
		[19.35063265312368586,	-99.28424288649109997],
		[19.35055345933599114,	-99.28426229689490867],
		[19.35062266010421794,	-99.28430542138914916],
		[19.3507882292278488,	-99.28442038735204278],
		[19.35086110410461302,	-99.28448697545407242],
		[19.35086729484126522,	-99.28448307666633355]
	];

	map.drawPolygon({
		paths: entrada_gas,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#e74c3c',
		fillOpacity: 0.6
	});
	map.drawPolygon({
		paths: peligroso_peaton,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#e74c3c',
		fillOpacity: 0.6
	});
	map.drawPolygon({
		paths: baldio_1,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#e74c3c',
		fillOpacity: 0.6
	});
	map.drawPolygon({
		paths: baldio_2,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#e74c3c',
		fillOpacity: 0.6
	});
	map.drawPolygon({
		paths: barranca,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#e74c3c',
		fillOpacity: 0.6
	});
	map.drawPolygon({
		paths: zona_deshabitada,
		strokeColor: '#1315f0',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#e74c3c',
		fillOpacity: 0.6
	});
	map.drawPolygon({
		paths: zona_peligro_vasco,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#e74c3c',
		fillOpacity: 0.6
	});
	map.drawPolygon({
		paths: cruce_peligroso,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#e74c3c',
		fillOpacity: 0.6
	});
}
function escuela () {
	uamc = [
		[19.35334963087668214,	-99.28239672938632054],
		[19.35289851807950257,	-99.28159104211410124],
		[19.35291936604529184,	-99.28110994680780266],
		[19.35198195612494132,	-99.28181944270080805],
		[19.35137532038892871,	-99.28221782679320029],
		[19.35099563761536245,	-99.28258028741645091],
		[19.35071357566883066,	-99.28296258327243606],
		[19.35057528128149684,	-99.2832407311461651],
		[19.35093534877251287,	-99.28348796702552193],
		[19.35100963760397619,	-99.28373622291231015],
		[19.35121078025425234,	-99.28367605165800569],
		[19.35145194407947855,	-99.28373698758453259],
		[19.35334963087668214,	-99.28239672938632054]
	];
	map.drawPolygon({
		paths: uamc,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#3498db',
		fillOpacity: 0.6
	});
	cele = [
		[19.35076721598521843,	-99.28520225415374512],
		[19.35093640802406156,	-99.28458223764145885],
		[19.35086480470808823,	-99.28449349773272559],
		[19.35062265603670539,	-99.28430802772396646],
		[19.35056327543639654,	-99.28431313754381904],
		[19.35052604560181422,	-99.28439126326071573],
		[19.35048101959755584,	-99.28470915866796531],
		[19.35050295014177024,	-99.28492291657885005],
		[19.35050038660195426,	-99.28498025160403984],
		[19.35051972055302016,	-99.2852721952620243],
		[19.35076721598521843,	-99.28520225415374512]
	];
	map.drawPolygon({
		paths: cele,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#3498db',
		fillOpacity: 0.6
	});
	conalep = [
		[19.35039791761357719,	-99.28406264262277148],
		[19.35046955773431776,	-99.28412792509949725],
		[19.35063512290978593,	-99.28424549711047575],
		[19.35081308981208537,	-99.28434224008847764],
		[19.35086054433586611,	-99.28405301793578985],
		[19.35099420552558058,	-99.28400633500659467],
		[19.35099955125217974,	-99.2837509221618717],
		[19.35091829149296316,	-99.28350839143296014],
		[19.35055002394126333,	-99.28329142900076931],
		[19.35052266562865597,	-99.28338521003455241],
		[19.35047759672718115,	-99.28373177539502592],
		[19.35039791761357719,	-99.28406264262277148]
	];
	map.drawPolygon({
		paths: conalep,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#3498db',
		fillOpacity: 0.6
	});
	xumi = [
		[19.35038577610918864,	-99.28217498109660255],
		[19.35059335364838518,	-99.28232129311084009],
		[19.35076689480798606,	-99.28208180793308202],
		[19.35062868982907602,	-99.2818678503276999],
		[19.35046029033891912,	-99.28198224006898442],
		[19.35038577610918864,	-99.28217498109660255]
	];
	map.drawPolygon({
		paths: xumi,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#3498db',
		fillOpacity: 0.6
	});
}
function supermercado () {
	superama = [
		[19.35624153855770047,	-99.27630100537551527],
		[19.35615086091090831,	-99.2764547816079812],
		[19.35584255653473207,	-99.27629139436099592],
		[19.35546624305047203,	-99.27694013784167737],
		[19.35596043768855523,	-99.27722846827751368],
		[19.35645009695400631,	-99.27642594856439473],
		[19.35624153855770047,	-99.27630100537551527]
	];
	map.drawPolygon({
		paths: superama,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#3498db',
		fillOpacity: 0.6
	});
	sams = [
		[19.3543327634543374,	-99.2771852187121624],
		[19.35377962255163098,	-99.27816554219407408],
		[19.35422848293582376,	-99.27842023407907845],
		[19.35479069015947573,	-99.27744471610442645],
		[19.3543327634543374,	-99.2771852187121624]
	];
	map.drawPolygon({
		paths: sams,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#3498db',
		fillOpacity: 0.6
	});
}
function otros () {
	conagua = [
		[19.35083041084864774,	-99.2843396637516804],
		[19.35101352068893021,	-99.28431131115621611],
		[19.35150818542219398,	-99.28437993333538714],
		[19.35161694947776567,	-99.28443485530978307],
		[19.35177279810889317,	-99.28443773177571074],
		[19.35187955613978161,	-99.28419291879433217],
		[19.35164460114604168,	-99.28415341656075555],
		[19.35145969190766024,	-99.28374911135229297],
		[19.35120498250615739,	-99.28368611855290737],
		[19.3510069728103673,	-99.28375093499296611],
		[19.35101398822033758,	-99.28401158193581466],
		[19.35088032296554772,	-99.28406087120505674],
		[19.35083041084864774,	-99.2843396637516804]
	];
	map.drawPolygon({
		paths: conagua,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#3498db',
		fillOpacity: 0.6
	});
}
function salud () {
	abc = [
		[19.35460540087319004,	-99.28267745359441676],
		[19.3551639166226046,	-99.28304852658742163],
		[19.35602909989423281,	-99.2834774732586709],
		[19.35618740204272825,	-99.28349338534302149],
		[19.35638042735551068,	-99.28345201592385649],
		[19.35661305861117043,	-99.28339507620124493],
		[19.35754395609642131,	-99.28292752279818956],
		[19.35777915413154915,	-99.28281063788992356],
		[19.35771260697577389,	-99.2826515294374019],
		[19.35777716166678175,	-99.28250046621876379],
		[19.35781697772178234,	-99.28234936030459323],
		[19.35781715548650084,	-99.2822346765252064],
		[19.35768870098939232,	-99.28211455863907986],
		[19.35631459398739551,	-99.28123643447737834],
		[19.35618603417758976,	-99.28118408550098195],
		[19.35610174612560641,	-99.28129862394700922],
		[19.35574465721215631,	-99.28185057356743926],
		[19.35535418369483551,	-99.28159447392097547],
		[19.35528476268596521,	-99.28169339860481557],
		[19.35499816654006722,	-99.28145311670189699],
		[19.35458122828421068,	-99.28231251524638878],
		[19.35453156537521124,	-99.28243232439945132],
		[19.35453628669917592,	-99.28257829116809319],
		[19.35460540087319004,	-99.28267745359441676]
	];
	map.drawPolygon({
		paths: abc,
		strokeColor: '#131540',
		strokeOpacity: 0,
		strokeWeight: 0,
		fillColor: '#3498db',
		fillOpacity: 0.6
	});
}

doc.on("ready", mapa);
doc.on("ready", fotos);
doc.on("change", ".js-control input", controles);